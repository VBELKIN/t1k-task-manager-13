package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Project;
import ru.t1k.vbelkin.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    Integer getSize();

    Task add(Task task);

    void clear();

    List<Task> findAll();

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void remove(Project project);

    Task removeById(String id);

    List <Task> findAllByProjectId(String projectId);

    Task removeByIndex(Integer index);

}
