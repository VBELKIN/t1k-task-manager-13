package ru.t1k.vbelkin.tm.api.controller;

import ru.t1k.vbelkin.tm.model.Task;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTask(Task task);

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void removeTaskById();

    void removeTaskByIndex();

    void updateTaskById();

    void updateTaskByIndex();

    void startTaskById();

    void startTaskByIndex();

    void completeTaskById();

    void completeTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();
}
