package ru.t1k.vbelkin.tm.api.repository;

import ru.t1k.vbelkin.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
